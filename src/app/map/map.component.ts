import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { AuthService } from 'src/services/auth.service';


let globalPosn:any={
  longitude:69.55,
  latitude: 22.75
}

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {


  latitude = globalPosn.latitude
  longitude = globalPosn.longitude

  constructor(private AuthService:AuthService){ }


  ngOnInit() {

    this.AuthService.myAuthObs.subscribe(data =>{
      console.log('at map',data)

      this.AuthService.dronePositionObs.subscribe(data =>{
        if(data){
          globalPosn=data
          this.changeCoordinates(map)
        }
        console.log('latitude',globalPosn.latitude)
        console.log('longitude',globalPosn.longitude)
      })


      // this.AuthService.landObs.subscribe(data =>{
      //   console.log("land subscribed")
      // })

    });

    (mapboxgl as any).accessToken = 'pk.eyJ1IjoicmFqYXRzaW5naDE3IiwiYSI6ImNsZmpqeXoxejAxbWozd29mZDRmODI2YXgifQ.G60Y58J0A0e1eKOToXseCQ';
    const map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [globalPosn.latitude, globalPosn.longitude],
      zoom: 10
    });
    map.addControl(new mapboxgl.NavigationControl());

    map.loadImage("/assets/drone.png", function(error, image) {
      if (error) {
        console.log(error);
        throw error;
      }
      // Add the loaded image to the style's sprite with the ID 'kitten'.
      if(image){
        map.addImage("my-image", image, { sdf: true });
      }
    });
    map.on("load", () => {

      map.addSource("point", {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: [
            {
              type: "Feature",
              geometry: {
                type: "Point",
                coordinates: [globalPosn.latitude, globalPosn.longitude]
              },
              properties:{}
            }
          ]
        }
      });

      map.addLayer({
        id: "test",
        type: "symbol",
        source: "point",
        layout: {
          "icon-image": "my-image",
          "icon-size": 0.1,
          "icon-rotate": 10
        },
      });
    });

    // map.on('load', () => {
    //   map.addSource('radar', {
    //   'type': 'image',
    //   'url': '/assets/drone.png',
    //   'coordinates': {lat, lng}
    //   });
    //   map.addLayer({
    //   id: 'radar-layer',
    //   'type': 'symbol',
    //   'source': 'point'
    //   });
    //   });

    // map.on('load', () => {

    //   map.loadImage(
    //     '/assets/drone.png',
    //     (error, image) => {
    //       if (error) throw error;
          
    //       // Add the image to the map style.
    //       if(image){
    //         map.addImage('cat', image);
    //       }
    //       console.log()
    //       // Add a data source containing one point feature.
    //       map.addSource('point', {
    //       'type': 'geojson',
    //       'data': {
    //         'type': 'FeatureCollection',
    //         'features': [
    //           {
    //             'type': 'Feature',
    //             'geometry': {
    //             'type': 'Point',
    //             'coordinates': [-77.4144, 25.0759],
    //             },
    //             "properties":{
    //               'title':'t',
    //               'description':'e'
    //             }
    //           }
    //         ]
    //         }
    //       });
          
    //       // Add a layer to use the image to represent the data.
    //       map.addLayer({
    //         'id': 'points',
    //         'type': 'symbol',
    //         'source': 'point', // reference the data source
    //         'layout': {
    //           'icon-image': 'cat', // reference the image
    //           'icon-size': 0.25
    //         }
    //       });
    //     }
    //   );

      // console.log('abcd')

      // const image = new Image(50, 50);

      // image.src = '/assets/drone.png';

      // map.addImage('my-image', image, {
      //   pixelRatio: window.devicePixelRatio,
      //   sdf: true
      // });


      // map.addLayer({
      //   id: 'my-layer',
      //   type: 'symbol',
      //   source: {
      //     type: 'geojson',
      //     data: {
      //       type: 'FeatureCollection',
      //       features: [
      //         {
      //           type: 'Feature',
      //           geometry: {
      //             type: 'Point',
      //             coordinates: [38, 11]
      //           },
      //           properties: {}
      //         }
      //       ]
      //     }
      //   },
      //   layout: {
      //     'icon-image': 'my-image',
      //     'icon-size': 0.5
      //   }
      // });

      // map.addLayer({
      //   id: 'my-layer',
      //   type: 'symbol',
      //   source: {
      //     type: 'geojson',
      //     data: {
      //       type: 'FeatureCollection',
      //       features: [
      //         {
      //           type: 'Feature',
      //           geometry: {
      //             type: 'Point',
      //             coordinates: [38, 11]
      //           },
      //           properties: {}
      //         }
      //       ]
      //     }
      //   },
      //   layout: {
      //     'icon-image': 'my-image',
      //     'icon-size': 0.5
      //   }
      // });
    // });
    
  }

  changeCoordinates(map:mapboxgl.Map){
    // console.log("changing cord",(map.getSource('point') as any)._data.features[0].geometry.coordinates);

    // if (map.getLayer('test')) map.removeLayer('test');
    // map.removeSource('point');
    
    // map.addSource("point", {
    //   type: "geojson",
    //   data: {
    //     type: "FeatureCollection",
    //     features: [
    //       {
    //         type: "Feature",
    //         geometry: {
    //           type: "Point",
    //           coordinates: [globalPosn.latitude, globalPosn.longitude]
    //         },
    //         properties:{}
    //       }
    //     ]
    //   }
    // });

    // map.addLayer({
    //   id: "test",
    //   type: "symbol",
    //   source: "point",
    //   layout: {
    //     "icon-image": "my-image",
    //     "icon-size": 0.1,
    //     "icon-rotate": 10
    //   },
    // });

    (map.getSource('point')as any).setData(
      {
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [globalPosn.latitude, globalPosn.longitude]
            },
            properties:{}
          }
        ]
      }
    );
    
    map.panTo([globalPosn.latitude, globalPosn.longitude]);
    console.log('abc'+globalPosn.latitude + globalPosn.longitude)

    console.log("changing cord",(map.getSource('point') as any)._data.features[0].geometry.coordinates);

  }

  takeOff(){
    this.AuthService.takeOffObs.subscribe(data =>{
      console.log({data})
      console.log("takeoff subscribed")
    })
  }

  setVelocity(){
    this.AuthService.setVelocityObs.subscribe(data =>{
      console.log("velocity set ",data)
    })
  }
}
