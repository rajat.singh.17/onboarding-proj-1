import { Component,OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  data:any;

  constructor(private AuthService:AuthService){ }

  ngOnInit(){
    this.AuthService.myAuthObs.subscribe(data => {
      this.data = data;
      console.log("myAuthObs subscribed ",this.data);
      // this.subsDroneGlobalPosition()
      // this.callDroneTakeOff()
    });
  }

  // subsDroneGlobalPosition(){
  //   this.AuthService.dronePositionObs.subscribe(data => {
  //     this.data = data;
  //     console.log("dronePositionObs subscribed ",this.data);
  //   })
  // }

  // callDroneTakeOff(){
  //   this.AuthService.takeOffObs.subscribe(data => {
  //     this.data = data;
  //     console.log("myTakeOffObs subscribed ",this.data);
  //   })
  // }

  

}
